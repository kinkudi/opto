import json
import webapp2
import urllib2
from Location import add

class Customer(webapp2.RequestHandler):
	def post(self):
		self.response.headers['access-control-allow-origin'] = '*'
		self.response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
		uri = self.request.POST['uri']
		curi = self.request.POST['curi']
		try:
			uri = urllib2.unquote(uri)
		except:
			uri = uri.replace('%3A',':')
		try:
			curi = urllib2.unquote(curi)
		except:
			curi = curi.replace('%3A',':')
		curi = curi.replace(' ','_')
		if len(curi) > 0:
			if curi[-1] != '+':
				curi = ''.join([curi,'+'])
			key = add(curi,uri)
		else:
			self.response.out.write("///enter_some_data")
		self.response.out.write(key)

	def get(self):
		self.redirect('http://op.to/')

app = webapp2.WSGIApplication([
	(r'/c/.*',Customer)
	])

def main():
	app.run()

if __name__ == "__main__":
	main()


