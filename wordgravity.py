import re
import json
import sys
import math

#based on the following formula
#assuming each word has a score (or (information) mass)
#the word gravity between two words is defined only within a segment
#it is directly proportional to the product of each of the two words masses
#and inversely proportional to the square of the sum of the masses of words separating them (i.e., the information (mass) respecting distance)
#we fix n words in the segment as planets if and only if they fulfill the massfull definition of a planet
#in other words we define an arbitrary mass threshold ratio, say, 25% or 30% of the sum of word masses in a segment
#and any words in a segment whose mass is equal or above this threshold become planets
#planets can fuse, of course


class Master(dict):
	def __init__(self,adict):
		self.update(adict)
		self.average()

	def average(self):
		self.avg = 0.0
		self.total = 0.0
		lself = len(self)
		if lself != 0:
			for k in self:
				self.avg += self[k]
				self.total += 1.0/self[k]
			self.avg /= lself

master = Master(json.load(open('web','r')))


class Segment:
	#abstract and represents a segment or a sequence of words that is the greatest boundary for NEs of all words within the segment
	def __init__(self,wordsArray,scoreProvider):
		self.seg = wordsArray
		self.ref = scoreProvider
		#print 'Segment: scoring'
		self.score()
		#print 'Segment: totaling'
		self.total()
		#print 'Segment: finding majors'
		self.fol = ''
		self.fixMajors()
		#we can comment this out or tune the scaling to change relevance to now
		#self.upvote_dates()
		#print 'Segment: gravitating'
		#print ' '.join(self.seg)
		self.gravitate()
		self.ne = None

	def __str__(self):
		output = []
		for word in self.seg:
			output.append(word)
		#return `' '.join(output)` + '\n Major: ' + `','.join([m[0] for m in self.majors])` + '\n Notes : ' + self.fol
		return `' '.join(output)` + '\n Unique Ratio: ' + `'%s' % float('%.2g' % self.uniqueratio)` + '\n Master Avg: ' + `'%s' % float('%.2g' % (self.masteravg))` + '\n Major Total: ' + `'%s' % float('%.2g' % self.majorstotal)` + '\n Major Avg: ' + `'%s' % float('%.2g' % (self.majorstotal/len(self.seg)))` + '\n Major: ' + `','.join([m[0] for m in self.majors])` + '\n'+ '\n'.join([' , '.join(['\t'+k,`v`,' '.join(n)]) for [k,v,n] in self.complete ]) + '\n Notes : ' + self.fol

	def score(self):
		self.scores = self.ref.requestScores(self.seg)

	def upvote_dates(self):
		date_threshold_of_relevance = 5.0 #years
		for word in self.seg:
			if re.match(r'\d{4}',word):
				word = re.findall(r'\d{4}',word)[0]
				from_now = date_threshold_of_relevance/abs(2013 - int(word) + 0.5)
				self.majorstotal *= from_now**2 		
	def total(self):
		self.total = 0.0
		self.scorelist = []
		self.lenbigs = 1.0/len(self.seg) 
		self.masteravg = 0.0
		self.uniques = {}
		for word in self.seg:
			if word in self.ref.scalescores:
				score = self.ref.scalescores[word]
				self.scorelist.append(score)
				self.total += score
			else:
				self.scorelist.append(0.0)
			if len(word) > 1:
				self.lenbigs += 1
			score = 0.5
			lword = word.lower()
			if lword in self.ref.master:
				score = self.ref.master[lword]
			self.masteravg += score
			if lword not in self.uniques:
				self.uniques[lword] = 1
			else:
				self.uniques[lword] += 1
		self.uniqueratio = 1.0*len(self.uniques)/self.lenbigs
		self.masteravg /= len(self.uniques)

	def fixMajors(self):
		self.majors = []
		self.majorstotal = 0.0
		for i in xrange(len(self.seg)):
			if len(self.majors) > 39:
				break
			word = self.seg[i]
			if word in self.ref.sigwords and word not in self.ref.tiny_words:
				self.majors.append((word,i))
				self.majorstotal += self.ref.scalescores[word]
			#identify word lists and phrases in other languages
		if len(self.seg) >= 3 and self.masteravg >= 0.1:
			self.fol += '\nAppears to be in a foreign language, or unusually dense with information'
			#print self.seg,self.fol
			self.majorstotal /= len(self.seg)
		if len(self.seg) >= 3 and len(self.majors) >= math.floor(1.0/(1.0 + math.exp(1.0))*len(self.uniques)):
			self.fol += '\nAppears to be a title, headline or contain a set of terms significant to the topic'
			#print self.seg,self.fol
			self.majorstotal *= self.lenbigs
		if len(self.seg) >=4 and self.uniqueratio <= 3.0/4.0:
			self.fol += '\nAppears to be highly repetitive, could be list of related terms'
			#print self.seg,self.fol
			self.majorstotal *= self.uniqueratio

	def heightsum(self,vlist,pivot):
		heightsum = len(vlist)*[0.0]
		vi = 0.0
		for i in range(pivot,-1,-1):
			vi += vlist[i][1]
			heightsum[i] = (vlist[i][0],vi)
		vi = 0.0
		for i in range(pivot,len(vlist)):
			vi += vlist[i][1]
			heightsum[i] = (vlist[i][0],vi)
		return heightsum
		
	def calculatePA(self,majorindex):
		major = self.seg[majorindex]
		left = sum(self.scorelist[:majorindex])
		right = self.scorelist[majorindex:]
		majorscore = self.ref.scalescores[major]
		using = left + 1.0
		switch = False
		pairwise = []
		dist = 0.5
		lwscore = 1.0
		for i in xrange(len(self.seg)):
			dist = (i - majorindex +0.5)
			if i == majorindex:
				using = 1.0
				switch = True
			word = self.seg[i]
			wscore = 1.0
			lword = word.lower()
			if lword in self.ref.master:
				wscore = self.ref.master[lword]
			elif len(re.findall(r'[\._-]',lword)) > 0:
				s = re.split(r'[\._-]',lword)
				score = 1.0
				for sw in s:
					if sw in self.ref.master:
						score *= self.ref.master[sw]
				wscore = score
			wscore = -math.log(wscore)
			if abs(wscore) == 0:
				wscore = 2.0*(1.0+math.exp(1.0))**2
			gravity = wscore#abs(wscore - lwscore)/wscore# / lwscore 
			#need heuristic
			#for NE
			#from main words
			lwscore = wscore
			if lwscore == 0.0:
				lwscore = 1.0
			pairwise.append((word,abs(gravity),gravity))
			if not switch:
				using -= wscore
			else:
				#print i,majorindex,i-majorindex,len(right)-1
				using += right[i-majorindex]
		heights = self.heightsum(pairwise,majorindex)
		return heights,pairwise
				
	def gravitate(self):
		#we calculate NE boundaries by looking at 
		#the scores the majors get with words
		#we extract the NEs by gravitating words to the major they are most attracted to
		#override this in order to do two types of gravitation
		#enclosed - where we respect the sequence of words and there is only 1 boundary between any two majors
		#free - minor words gravitate toward whichever majors they are most attracted to, regardless of sequenctial position in segment
		pass		

class EnclosedSegment(Segment):
	def gravitate(self):
		self.complete = [] 
		for majorentry in self.majors:
			major = majorentry[0]
			majorindex = majorentry[1]
			pairwiseattract = self.calculatePA(majorindex)
			self.complete.append([major, pairwiseattract])
			

class JSegment(Segment):

	def wscore(self,word):
		#return self.scores[word]
		if word in self.ref.scalescores:
			return self.ref.scalescores[word]
		else:
			return 0.0
		lword = word.lower()
		wscore = 1.0
		if lword in self.ref.master:
			wscore = self.ref.master[lword]
		elif len(re.findall(r'[\._-]',lword)) > 0:
			s = re.split(r'[\._-]',lword)
			score = 1.0
			for sw in s:
				if sw in self.ref.master:
					score *= self.ref.master[sw]
			wscore = score
		wscore = -math.log(wscore)
		if abs(wscore) == 0:
			wscore = 2.0*(1.0+math.exp(1.0))**2
		return wscore

	def gravitate(self):
		TH = 99.995
		self.complete = []
		self.ll = []
		for s in self.seg:
			self.ll.append([s,self.wscore(s)])
		for me in self.majors:
			major = me[0]
			mi = me[1]
			ne = [major]
			for i in range(mi+1,len(self.seg)):	
				e = self.ll[i]
				word = e[0]
				wscore = e[1]
				if wscore > TH:
					ne.append(word)
				else:
					break
			for i in range(mi-1,-1,-1):
				e = self.ll[i]
				word = e[0]
				wscore = e[1]
				if wscore > TH:
					ne.insert(0,word)
				else:
					break
			#print major,' ==> ',' '.join(ne)
			#print major,' : ',ne
			#i = sys.stdin.readline()
			self.complete.append([major,mi,ne])

class FrequencyCount(dict):
		#a modified frequency count
		#where we report the proportion of total words a word represents
		#scaled by a function that depends on where that word occurs in the file, it's posssocre
		#the highest scoring words are the most frequent words that occur toward the start
		def __init__(self,wordsArray):
			super(dict, self).__init__()
			if len(wordsArray) == 0:
				return
			self.freqdelta = 1.0/len(wordsArray)
			self.posdelta = -self.freqdelta
			self.posscore = 1.618
			for word in wordsArray:
				self.count(word)
				self.posscore += self.posdelta

		def count(self,word):
			word = word.lower()
			if word in self:
				self[word] += self.posscore*self.freqdelta
			else:
				self[word] = self.posscore*self.freqdelta

		def freq(word):
			word = word.lower()
			if word in self:
				return self[word]
			return -1

class ScoreProvider(object):
	#provides scores for our words
	def __init__(self,dictionary,raw_words,reverse):
		self.tiny_words = { 'by': 1, 'to':1, 'in':1, 'for':1, 'of':1, 'as':1, 'so':1, 'a':1,'the':1}
		self.reverse = reverse
		self.NEratio = 1.0/(2*math.exp(1.0)) #the larger this is the more words are part of NEs
		self.word_count = len(raw_words)
		if self.word_count == 0:
			raw_words.append('internet')
			self.word_count = 1.0
		self.master = dictionary
		self.nullscore = self.master.avg*self.master.avg
		self.local = FrequencyCount(raw_words)
		self.mix = {}
		self.globalscores = self.requestScores(raw_words)
		self.vectorize_mix()
		self.normalize1scores()
		self.normalize2scores()
		self.scalescores()
		self.sigwords()
		#self.show_scores()
		#i = sys.stdin.readline()

	def requestScores(self,wordsArray):
		#gets scores for a segment
		#using local and master
		self.scores = {}
		for word in wordsArray:
			lword = word.lower()
			score_tuple = [self.local[lword],self.nullscore]
			if lword in self.master:
				score_tuple[1] = self.master[lword]
			self.scores[word] = score_tuple
		return self.scores

	def vectorize_mix(self):
		self.words = []
		self.wordscore = []
		for item in self.mix.iteritems():
			self.words.append(item[0])
			self.wordscore.append(item[1])
		self.sortedscores = sorted(self.mix.iteritems(),key=lambda(x):x[1],reverse=True)

	def normalize1scores(self):
		norm1 = sum(self.wordscore)
		if norm1 == 0:
			norm1 = 1.0
		if not self.reverse:
			self.norm1scores = dict([(self.words[i],1.0-(self.wordscore[i]/norm1)) for i in xrange(len(self.wordscore))])
		else:
			self.norm1scores = dict([(self.words[i],self.wordscore[i]/norm1) for i in xrange(len(self.wordscore))])
		self.sortednorm1scores = sorted(self.norm1scores.iteritems(),key=lambda(x):x[1],reverse=True)

	def normalize2scores(self):
		norm2 = math.sqrt(sum([self.wordscore[i]**2 for i in xrange(len(self.wordscore))]))
		if norm2 == 0:
			norm2 = 1.0
		if not self.reverse:
			self.norm2scores = dict([(self.words[i],1.0-(self.wordscore[i]/norm2)) for i in xrange(len(self.wordscore))])
		else:
			self.norm2scores = dict([(self.words[i],self.wordscore[i]/norm2) for i in xrange(len(self.wordscore))])
		self.sortednorm2scores = sorted(self.norm2scores.iteritems(),key=lambda(x):x[1],reverse=True)


	def scalescores(self):
		smin = min(self.wordscore)
		smax = max(self.wordscore)
		if smax == 0:
			smax = 1.0
		if not self.reverse:
			self.scalescores = dict([(self.words[i],100.0-100.0*((self.wordscore[i]-smin)/smax)) for i in xrange(len(self.wordscore)) if re.match(r'\w+',self.words[i]) and not re.match(r'\d+',self.words[i])])
		else:
			self.scalescores = dict([(self.words[i],100.0*(self.wordscore[i]-smin)/smax) for i in xrange(len(self.wordscore)) if re.match(r'\w+',self.words[i]) and not re.match(r'\d+',self.words[i])])
		self.sortedscalescores = sorted(self.scalescores.iteritems(),key=lambda(x):x[1],reverse=True)

	def sigwords(self):
		siglen = int(math.ceil(len(self.sortedscalescores)*self.NEratio))
		self.sigwords = dict([(s[0],s[1]) for s in self.sortedscalescores[:siglen]])

	def show_scores(self):
		print 'Original scores'
		for i in xrange(len(self.sortedscores)):
			print self.sortedscores[i],self.sortedscalescores[i],self.sortednorm1scores[i],self.sortednorm2scores[i]




class MasterOnly(ScoreProvider):
	def requestScores(self,wordsArray):
		return self.master

class LocalOnly(ScoreProvider):
	def requestScores(self,wordsArray):
		return self.local

class SimpleMixed(ScoreProvider):
	#lowest are most likely NEs
	def requestScores(self,wordsArray):
		super(SimpleMixed,self).requestScores(wordsArray)
		self.mix = {}
		for word in self.scores:
			scores = self.scores[word]
			self.mix[word] = -math.log(scores[0] * scores[1])
		return self.mix

class ModerateMixed1(ScoreProvider):
	#closest to 1 are most likely NEs
	def requestScores(self,wordsArray):
		super(ModerateMixed1,self).requestScores(wordsArray)
		self.mix = {}
		rescale = math.log(-math.log(self.word_count/self.master.total))
		for word in self.scores:
			scores = self.scores[word]
			lscore = scores[0]**rescale
			lscore = -math.log(lscore)*lscore
			mscore = (1.0/scores[1])/self.master.total
			ascore = -math.log(mscore)*mscore
			result = math.sqrt(mscore**2 + lscore**2)/(abs(mscore - lscore))
			self.mix[word] = result
		return self.mix

class ModerateMixed2(ScoreProvider):
	#closest to 1 are most likely NEs
	def requestScores(self,wordsArray):
		super(ModerateMixed2,self).requestScores(wordsArray)
		self.mix = {}
		rescale = math.log(-math.log(self.word_count/self.master.total))
		for word in self.scores:
			scores = self.scores[word]
			lscore = scores[0]**rescale
			lscore = -math.log(lscore)*lscore
			mscore = (1.0/scores[1])/self.master.total
			mscore = -math.log(mscore)*mscore
			result = math.sqrt(abs(mscore**2 - lscore**2))/(abs(mscore - lscore))
			self.mix[word] = result
		return self.mix

class ModerateMixed3(ScoreProvider):
	#smallest are most likely ***relevant*** NEs
	def requestScores(self,wordsArray):
		super(ModerateMixed3,self).requestScores(wordsArray)
		self.mix = {}
		rescale = math.log(-math.log(self.word_count/self.master.total))
		for word in self.scores:
			scores = self.scores[word]
			lscore = scores[0]**rescale
			lscore = -math.log(lscore)*lscore
			mscore = (1.0/scores[1])/self.master.total
			mscore = -math.log(mscore)*mscore
			result = math.sqrt(abs(math.exp(mscore) + math.exp(lscore)))/(abs(mscore - lscore)*math.sqrt(scores[1]))
			self.mix[word] = result
		return self.mix

class ComplexMixed1(ScoreProvider):
	#highest are most likely NEs and but doesn't work so well on larger scales
	def requestScores(self,wordsArray):
		#first we will just return tuples of local and master
		super(ComplexMixed1,self).requestScores(wordsArray)
		self.mix = {}
		#rescale master dict and local to similar size
		rescale = math.log(-math.log(self.word_count/self.master.total))
		for word in self.scores:
			scores = self.scores[word]
			#kind of entropy
			fscore = scores[0]*scores[0]*len(word)
			localscore = -math.log(fscore)*fscore
			mscore = (1.0/scores[1])/self.master.total
			masterscore = -math.log(mscore)*mscore
			result = math.sqrt(abs(localscore**rescale- masterscore))/math.sqrt(localscore*masterscore)		
			self.mix[word] = result
		return self.mix

class ComplexMixed2(ScoreProvider):
	#highest are most likely NEs and rarer words (particularly on larger scales)
	def requestScores(self,wordsArray):
		#first we will just return tuples of local and master
		super(ComplexMixed2,self).requestScores(wordsArray)
		self.mix = {}
		#rescale master dict and local to similar size
		rescale = math.log(-math.log(self.word_count/self.master.total))
		for word in self.scores:
			scores = self.scores[word]
			#kind of entropy
			fscore = scores[0]*scores[0]*len(word)
			localscore = -math.log(fscore**rescale)*fscore
			mscore = (1.0/scores[1])/self.master.total
			masterscore = -math.log(mscore)*mscore
			result = math.sqrt(abs(localscore**2- masterscore**2))/math.sqrt(localscore*masterscore)		
			self.mix[word] = result
		return self.mix

class Segmenter:
	#segments a text file into segments based on regexes provided by MatchProvider
	def __init__(self,toSegment,dictionary,matchProvider,scoreProvider,reverse):
		#print 'Reading file'
		self.rawtext = toSegment#,'r')).read().replace('--', ' , ').replace('\xa1\xa6','\'')
		#print self.rawtext
		#i = sys.stdin.readline()
		self.matcher = matchProvider
		self.reverse = reverse
		#print 'Getting words'
		self.raw_words = self.matcher.tokens.findall(self.rawtext,re.UNICODE)
		#print 'Processingjoined words'
		#self.raw_words = self.split_camelCase_words(self.raw_words)
		self.scoreProvider = scoreProvider
		#print 'Getting scores'
		self.scorer = self.getScoreProvider(dictionary)
		self.non_breaking_abbreviations = { 
				#abbrevs that usually involve a stop but don't end the segment
				'mr':1,'dr':1,'ms':1,'mrs':1,'sr':1,'jr':1,'hon':1,'mp':1,'lt':1,'co':1,'gen':1,'gov':1,'no':1,'nom':1,'vol':1,
				'addr':1,'tel':1,'fl':1,'il':1,'ma':1,'mme':1,'mssrs':1,'mlle':1,'mil':1,'km':1,'kilo':1,'oz':1,'lt':1,'ca':1,'ny':1,
				'mi':1,'fr':1,'sen':1,'esq':1,'inc':1,'pty':1,'ltd':1,'corp':1,'llc':1,'st':1,'rd':1,'th':1
		}
		#print 'Segmenting'
		self.segs = self.segment()
		self.refined_segs = self.discard_weak_segs()
		#print 'Sorting segs'
		self.sortsegs()
		self.sortNEs()

	def process(self):
		text = []
		for wd in self.raw_words:
			if wd in self.scorer.scalescores:
				text.append(''.join([wd,'(',`round(self.scorer.scalescores[wd],4)`,')']))
			else:
				text.append(''.join([wd,'(0.0)']))
		return self.rawtext +' '.join(text)


	def splitCamel(self,word):
		splits = []
		lindex = 0
		index = 0
		if re.match(r'[\d\w]+',word):
			return [word]
		for l in word:
			if l.isupper() and ((index > 0 and not word[index-1].isupper()) or (index < len(word)-1 and not word[index+1].isupper())) and not re.match(r'[,\'\."_-]',word[index-1]):
				if lindex != index:
					splits.append(word[lindex:index])
				lindex = index
			elif l.isdigit() and index > 0 and not re.match(r'\d',word[index-1]):
				if lindex != index:
					splits.append(word[lindex:index])
				lindex = index
			index += 1
		if lindex != index:
			splits.append(word[lindex:index])
		return splits						

	def split_camelCase_words(self,wordlist):
		uncameled = []
		for word in wordlist:
			dehumped = self.splitCamel(word)
			uncameled.append(dehumped)
		return [item for sublist in uncameled for item in sublist]

	def sortNEs(self):
		self.ne = {}
		for seg in self.segs:
			for e in seg.complete:
				ne = ' '.join(e[2])
				score = 0.0
				for new in e[2]:
					if new in self.scorer.scalescores:
						score += self.scorer.scalescores[new]				
				if ne in self.ne:
					self.ne[ne][0] += 1
				else:
					self.ne[ne] = [1,score]
		self.sortedne = sorted(self.ne.iteritems(),key=lambda(x):x[1][0]*len(x[0]),reverse=True)

	def sortsegs(self):
		self.sortedsegs = sorted(self.segs,key=lambda(x):x.majorstotal,reverse=True)
		
	def ends_a_segment(self,word,word_index):
		#cut and pasted this in from a previous version excuse the mess
		match = self.matcher.breaks.match(word)
		if match:
			#caveats for some conditions involving break words 
			#print word
			#i = sys.stdin.readline()
			if len(word) > 1 and match.start() < len(word)-1: #i.e. there is another stop in the word like U.S.
				#print "Another stop in this word"
				return False
			elif word_index > 0: #check the previous word if it exists
				lword = self.raw_words[word_index-1].lower()
				#print lword
				if (len(self.matcher.breaks.findall(lword)) > 0 and len(lword) > 1) or len(lword) == 1 and re.match(r'\w+',lword):	
					#i.e. so either an initial like J. Ito or 
					#either stop itself or contains stop like B.I.G in this case probably not a sentence end
					#print "Initial"
					return False
				if lword in self.non_breaking_abbreviations:
					#print "NBabbrev"
					return False
			elif word_index < len(self.raw_words)-1: #check the next word if it exists
				nword = self.raw_words[word_index+1]
				if self.matcher.breaks.match(word): #so ., or an ellipsis tridot :)
					#print "Continuer"
					return False
			#passed all the caveats so
			#print "Ender"
			return True
		else:
			#print "Not even a break word"
			return False

	def getScoreProvider(self,dictionary):
		return self.scoreProvider(dictionary,self.raw_words,self.reverse)

	def segment(self):
		#split the whole input into tokens
		#make a new seg in (non-empty) gaps of break matching tokens
		#always exclude tokens that match exclude
		segs = []
		seg = []
		for word_index in xrange(len(self.raw_words)):
			word = self.raw_words[word_index]
			result = self.ends_a_segment(word,word_index)
			if result or len(seg) > 78:
				if len(seg) > 78:
					pass
					#print seg
					#i = sys.stdin.readline()
				candidate = [s for s in seg if not self.matcher.excludes.match(s)]
				if len(candidate) > 0:
					segs.append(JSegment(candidate,self.scorer))
				seg = []
			elif not self.matcher.excludes.match(word):
				seg.append(word)
		candidate = [s for s in seg if not self.matcher.excludes.match(s)]
		if len(candidate) > 0:
			segs.append(JSegment(candidate,self.scorer))
		return segs

	def discard_weak_segs(self):
		#Depending on the type of scoring we have different definitions of weak
		#should normalize score systems
		better_segs = []
		for seg in self.segs:
			pass
		return better_segs

class MatchProvider(object):
	#provides regex matching 
	def __init__(self):
		self.tokenizes = None
		self.breaks = None
		self.excludes = None

	def defineNewTokensMatch(self,regexString):
		self.tokens = re.compile(regexString,re.UNICODE)
	
	def defineNewBreaksMatch(self,regexString):
		self.breaks = re.compile(regexString,re.UNICODE)

	def defineNewExcludesMatch(self,regexString):
		self.excludes = re.compile(regexString,re.UNICODE)

class Normal(MatchProvider):
	"""The normal one"""
	def __init__(self):
		super(MatchProvider, self).__init__()
		self.defineNewTokensMatch( r"\w+[\w\.&,-]+\w|\w\w|\w|[^\w\s]|\s+" )
		self.defineNewBreaksMatch( r"[^\w()'$&\"!,:% -]+")
		self.defineNewExcludesMatch( r"[^\w():$&,'%]+" )

def make_scores(nelist,ne_eval):
	nemass = {}
	score = {}
	stats = {}
	total_possible_score = 0.0
	links = {}
	if ne_eval is None:
		return {},1.0
	nee_eval = {}
	for ne in ne_eval:
		ne = ne.replace('.','')
		nee_eval[ne] = 1
	for ne in nee_eval:
		ne = ne.replace('.','')
		s = re.split(r'[\' ]+', ne)
		splits = [' '.join(s[a:b]) for b in xrange(len(s)+1) for a in xrange(b)] 
		for sp in splits:
			if sp not in links:
				links[sp] = {}
			splinks = links[sp]
			splinks[ne] = 1
			if sp in nee_eval:
				continue
			if sp not in nemass:
				nemass[sp] = 0.5
				total_possible_score += 0.5
		if ne not in nemass:
			nemass[ne] = 1.0
			total_possible_score += 1.0

	counted = {}
	tscore = 0.0
	accuracy = 0.0
        for ne in nelist:
		s = re.split(r'[\' ]+', ne)
		nescore = 0.0
		splits = [' '.join(s[a:b]) for b in xrange(len(s)+1) for a in xrange(b)]
		for sp in splits:#need a dict of NEs connected for each part in nemass otherwise we are marking some as not found when in fact we found part of them
			if sp in nelist:
				continue
			if sp in links:
				for nel in links[sp].keys():
					if nel not in stats:
						stats[nel] = ['Found', []]
					if sp == nel:
						stats[nel][1].append(['Full',sp,1.0*len(nel)/len(ne)])
						if ne not in score or score[ne] < 1.0:
							score[ne] = 1.0
					else:
						stats[nel][1].append(['Part',sp])
						if ne not in score or score[ne] < 1.0:
							score[ne] = 0.5	
			if sp in nemass:
				if sp not in counted:
					if ne not in score or score[ne] < 1.0:
						score[ne] = 0.5
					counted[sp] = 1
					tscore += 0.5
		if ne not in score:
			score[ne] = 0.0
		if ne not in counted:
			counted[ne] = 1
			if ne in nemass:
				if ne not in stats:
					stats[ne] = ['Found', []]
				if ne not in score or score[ne] < 1.0:
					score[ne] = 1.0
				stats[ne][1].append( ['Full',ne,1.0] )
				tscore += 1.0
	tscore = 0.0
	fulls = 0
	for j in stats.iteritems():
		jscore = 0.0
		ne = j[0]
		jlist = j[1][1]
		for t in jlist:
			if t[0] == 'Full' and t[1] in ne_eval:
				accuracy += t[2]
				fulls += 1
				jscore = 1.0
				break
		if jscore == 1.0:
			tscore += 1.0
		else:
			tscore += 0.5
	if fulls > 0:
		accuracy = 100.0 *accuracy / fulls
	percent = 100.0 * tscore / len(ne_eval) 
        return score,stats,percent,accuracy

def score_nes(nelist,ne_eval):
        scores = []
	sscores = []
	nee_eval = {}
	for ne in ne_eval:
		nee_eval[ne.replace('.','')] = 1
	ne_eval = nee_eval
        score,stats,overall,acc = make_scores(nelist,ne_eval)
        for ne in nelist:
                if len(ne) > 0:
                        scores.append([ne,score[ne]])
                else:
                        scores.append(['<h3>Found NEs (by significance)</h3>',len(nelist)-1])
	sscores.append(['<h3>Annotated NEs (in given order)</h3>',len(ne_eval)])
	for ne in ne_eval:
		ne = str(ne)
		if ne not in stats:
			sscores.append([ne, '<u>Not found</u>'])
		else:
			sscores.append([ne,''.join([
				'<b>Found ? </b>', ' : '.join([stats[ne][0],`stats[ne][1]`])
					])
				])
        return scores,sscores,overall,acc

def jmain(rawtext,num=5,NEonly=False,ne_eval=None):
	matcher = Normal()
	m3 = Segmenter(rawtext,master,matcher,ModerateMixed3,False)
	alltrials = [m3]
	words = ['']
	summary = ['']
	nes = ['']
	names = ['M3']
	for i in xrange(min(num,len(m3.scorer.sortedscalescores))):
		ranki = [x.scorer.sortedscalescores[i][0]  for x in alltrials]
		for trial in ranki:
			words.append(trial)
	for si in xrange(min(num,len(m3.sortedsegs))):	
		for ni in xrange(len(names)):
			name = names[ni]
			segger = alltrials[ni]
			summary.append(' '.join(segger.sortedsegs[si].seg))
			#print >>fo,  '\n\n',name,'\n',segger.sortedsegs[si]
	for si in xrange(min(num,len(m3.sortedne))):	
		for ni in xrange(len(names)):
			name = names[ni]
			segger = alltrials[ni]
			if si < len(segger.sortedne):
				nes.append(segger.sortedne[si][0])
				#print >>fo,  '\n\n',name,'\n',segger.sortedne[si]
		#i = sys.stdin.readline()

	#"""
        if NEonly:
                scorednes,stats,matchscores,acc = score_nes(nes,ne_eval)
                return ''.join(['<ol>',
				'</li><li>\t'.join([','.join([x[0],`x[1]`]) for x in scorednes ]),
				'</ol><p><ol>',
				'</li><li>\t'.join([' '.join([x[0],`x[1]`]) for x in stats ]),		
				'</ol><p>'
				]),matchscores,acc,m3
        else:
	        output = '<br><br> '.join([ 	'\n'.join(['\n<h1>Summary</h1><ol>','\n <p></li><li>\t'.join(summary)]),'</li></ol>'
				'\n'.join(['<h1>Phrases rich in relevant terms</h1>','\n <p>'.join(nes)]),
				'\n'.join(['<h1>Important words in this text</h1>','\n <p>'.join(words)])
			])
	return output

if __name__ == "__main__":
	print jmain(open(sys.argv[1],'r').read())


