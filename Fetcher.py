import urllib2


def uqu(uri):
	try:	
		uri = urllib2.unquote(uri)
	except:
		uri = uri.replace('%3A',':')
	return uri

def getHead(uri):
	tries = 3		
	while tries > 0:
		try:
			result = urlfetch.fetch(uri, method=urlfetch.HEAD, allow_truncated=True, deadline=3)
		except:
			return None
		if result.status_code == 200:
			return result.headers
		else:
			return None
		tries -= 1

def getBody(uri):
	if uri is None:
		return None
	else:
		tries = 3		
		while tries > 0:
			try:
				result = urlfetch.fetch(uri, allow_truncated=True, deadline=3)
			except:
				return None
			if result.status_code == 200:
				return result
			tries -= 1

