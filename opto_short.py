import urllib2
import webapp2
from Location import addRandom

class Shortener(webapp2.RequestHandler):
	def post(self):
		self.response.headers['Access-Control-Allow-Origin'] = '*'
		self.response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
		uri = self.request.POST['uri']
		try:
			uri = urllib2.unquote(uri)
		except:
			uri = uri.replace('%3A',':')
		key = addRandom(uri)
		self.response.out.write(key)
	
	def get(self):
		self.redirect('http://op.to/')

app = webapp2.WSGIApplication([
	(r'/s/.*',Shortener)
	])

def main():
	app.run()

if __name__ == "__main__":
	main()




