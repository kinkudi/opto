from google.appengine.api import urlfetch
import Location as loc
import webapp2
import urllib2
import re

class Usher(webapp2.RequestHandler):
	def handleOptomized(self,uri):
		self.response.headers['Content-Type'] = 'text/html'
		lh = loc.alreadyExists(uri)
		if lh is None:
			self.response.out.write("""
<head><title>op.to - rewire your web</title>
<link href='http://op.to/styles/crazykolors.css' rel='stylesheet' type='text/css' />
</head><body>
<h2>Rewiring you now . . . <br><a href='http://op.to/'>http://op.to/<br>Links that Rock<br>Use responsibly.</a></h2>
<img id=bgimg src='http://op.to/opto-basic-2.png' />
<script>
""")
			self.response.out.write(''.join(['\nvar redirect = function () { window.location.href=\''+'http://op.to/'+'\';\n};setTimeout(redirect,50);</script></body>']))
			return
		else:
			lhhref = lh.href
			if lhhref[:4] != 'http':
				lhhref = '://'.join(['http',lhhref])
			lhsummary = None
			try:
				lhsummary = lh.summary
			except:
				self.response.out.write("""
<head><title>op.to - rewire your web</title>
<link href='http://op.to/styles/crazykolors.css' rel='stylesheet' type='text/css' />
</head><body>
<h2>Rewiring you now . . . <br><a href='http://op.to/'>http://op.to/<br>Links that Rock<br>Use responsibly.</a></h2>
<img id=bgimg src='http://op.to/opto-basic-2.png' />
<script>
""")
				self.response.out.write(''.join(['\nvar redirect = function () { window.location.href="',lhhref,'";\n};setTimeout(redirect,50);</script></body>']))
				return
			if lhsummary is not None and len(lhsummary) > 5:
				self.response.out.write('<head><title>opto summary</title><head><body>')
				if len(lhhref) > 32:
					self.response.out.write('<a href='+lhhref+'>To proceed to the original site ('+lhhref[:32]+'...) Click here</a>')
				else:
					self.response.out.write('<a href='+lhhref+'>To proceed to the original site ('+lhhref+'...) Click here</a>')
				self.response.out.write(''.join([lhsummary,'</body>']))
				return
			else:
				self.response.out.write("""
<head><title>op.to - rewire your web</title>
<link href='http://op.to/styles/crazykolors.css' rel='stylesheet' type='text/css' />
</head><body>
<h2>Rewiring you now . . . <br><a href='http://op.to/'>http://op.to/<br>Links that Rock<br>Use responsibly.</a></h2>
<img id=bgimg src='http://op.to/opto-basic-2.png' />
<script>
""")
				self.response.out.write(''.join(['\nvar redirect = function () { window.location.href="',lhhref,'";\n};setTimeout(redirect,50);</script></body>']))
				return

	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		qs = self.request.query_string
		uri = self.request.path.lstrip('/')
		if qs is not None and len(qs) > 0: #if the uri contains ?, add
			uri = ''.join([uri,'?',qs])
		try:
			uri = urllib2.unquote(uri)
		except:
			uri = uri.replace('%3A',':')
		uri = uri.strip()
		if uri[0:5] == 'mized':
			self.handleOptomized(uri)
			return 
		lh = loc.alreadyExists(uri)
		if lh is None: #try a fetch
			if 'http' != uri[:4]:
				uri = '://'.join(['http',uri])
			try:
				result = urlfetch.fetch(uri,method=urlfetch.HEAD,allow_truncated=True,deadline=3)
				lh = loc.radd(uri,uri) #if we're here add it
			except:
				self.redirect(str('http://op.to/'))
				return
		uagent = self.request.headers['User-Agent']
		if uagent is None:
			uagent = ''
		else:
			uagent = uagent.lower()
		if len(uagent) == 0 or 'facebook' in uagent or 'bot' in uagent or 'urllib' in uagent or 'http' in uagent or 'www' in uagent or 'postrank' in uagent or 'expand' in uagent:
			self.redirect(str(lh.href)) #302 me if i'm a bot
			return
		lhhref = lh.href
		if lhhref[:4] != 'http':
			lhhref = '://'.join(['http',lhhref])
		self.response.out.write("""
<head><title>op.to - rewire your web</title>
<link href='http://op.to/styles/crazykolors.css' rel='stylesheet' type='text/css' />
</head><body>
<h2>Rewiring you now . . . <br><a href='http://op.to/'>http://op.to/<br>Links that Rock<br>Use responsibly.</a></h2>
<img id=bgimg src='http://op.to/opto-basic-2.png' />
<script>
""")
		self.response.out.write(''.join(['\nvar redirect = function () { window.location.href="',lhhref,'";\n};setTimeout(redirect,50);</script></body>']))

	def head(self):
		self.response.headers['Content-Type'] = 'text/html'
		qs = self.request.query_string
		uri = self.request.path.lstrip('/')
		if qs is not None and len(qs) > 0: #if the uri contains ?, add
			uri = ''.join([uri,'?',qs])
		try:
			uri = urllib2.unquote(uri)
		except:
			uri = uri.replace('%3A',':')
		uri = uri.strip()
		lh = loc.alreadyExists(uri)
		if lh is None: #try a fetch
			if 'http' != uri[:4]:
				uri = '://'.join(['http',uri])
			try:
				result = urlfetch.fetch(uri,method=urlfetch.HEAD,allow_truncated=True,deadline=3)
				lh = loc.radd(uri,uri) #if we're here add it
			except:
				self.redirect(str('http://op.to/'))
				return
		uagent = self.request.headers['User-Agent']
		if uagent is None:
			uagent = ''
		else:
			uagent = uagent.lower()
		if len(uagent) == 0 or 'facebook' in uagent or 'bot' in uagent or 'urllib' in uagent or 'http' in uagent or 'www' in uagent or 'postrank' in uagent or 'expand' in uagent:
			self.redirect(str(lh.href)) #302 me if i'm a bot
			return
		lhhref = lh.href
		if lhhref[:4] != 'http':
			lhhref = '://'.join(['http',lhhref])
		self.response.set_status(200)
		self.response.clear()

app = webapp2.WSGIApplication([
	(r'/.+',Usher)
	])

def main():
	app.run()


if __name__ == "__main__":
	main()



