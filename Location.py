import random
import string
from google.appengine.ext import db

rsrc = string.ascii_letters + string.digits + '/.$+,=~[]-_*^&`!"'

#this class is the data model for the uri
#the methods take care of adding under a specified key and adding under a random key
#each method ensure (barring contention issues since we assume they do not exist and currently offer no handling of them)
#that the given uri is assigned into the databased with a key either being the given key or the key returned
#users of these methods should thus check the return value


class Location(db.Model):
	href=db.TextProperty()
	count=db.IntegerProperty(indexed=False)
	summary=db.TextProperty()


def safety(key):
	ukey = []
	for kc in key:
		try:
			ukc = kc.encode('utf-8')
		except:
			continue
		ukey.append(ukc)
	if len(ukey) > 0 and ukey[-1] != '+':
		ukey.append('+')
	key = ''.join(ukey)
	if len(key) == 0:
		key = ''.join([random.choice(rsrc) for x in xrange(3)]) + '+'
	return key

def alreadyExists(key):
	lh = Location.get_by_key_name(key)
	if lh is None:
		real_key_try = ''.join(['/',key])
		lh = Location.get_by_key_name(real_key_try)
		if lh is not None:
			iadd(key,lh.href,lh.summary)
	if lh is None:
		real_key_try = ''.join([key,'+'])
		lh = Location.get_by_key_name(real_key_try)
		if lh is not None:
			iadd(key,lh.href,lh.summary)
	return lh

def iadd(key,uri,summ=None):
	key = safety(key)
	lh = Location(key_name=key)
	if 'http' != uri[:4]:
		uri = ''.join(['http://',uri])
	lh.href = db.Text(uri)
	lh.count = 1
	if summ is not None:
		lh.summary = db.Text(summ)
	else:
		lh.summary = 'Summary unavailable'
	lh.put()
	return key

def radd(key,uri,summ=None):
	key = safety(key)
	lh = Location(key_name=key)
	if 'http' != uri[:4]:
		uri = ''.join(['http://',uri])
	lh.href = db.Text(uri)
	lh.count = 1
	if summ is not None:
		lh.summary = db.Text(summ)
	else:
		lh.summary = 'Summary unavailable'
	lh.put()
	return lh

def add(key,uri,summ=None):
	key = safety(key)
	lh = alreadyExists(key)
	if lh is not None:
		return addNext(lh,key,uri,summ)
	lh = Location(key_name=key)
	if 'http' != uri[:4]:
		uri = ''.join(['http://',uri])
	lh.href = db.Text(uri)
	lh.count = 1
	if summ is not None:
		lh.summary = db.Text(summ)
	else:
		lh.summary = 'Summary unavailable'
	lh.put()
	return key

def addNext(existing,collidingKey,assigningUri,summ=None):
	collidingKey = safety(collidingKey)
	count = existing.count
	existing.count += 1
	existing.put()
	count += 1
	if collidingKey[-1] == '+':
		hopefullyClearKey = ''.join([collidingKey,str(count)])
	else:
		hopefullyClearKey = '+'.join([collidingKey,str(count)])
	iadd(hopefullyClearKey,assigningUri,summ) # use iadd to avoid stack loops, hoping for the best
	return hopefullyClearKey
	
def addRandom(uri):
	randkey = ''.join([random.choice(rsrc) for x in xrange(2)]) + '+'
	lh = alreadyExists(randkey)
	while lh is not None:
		if lh.href == uri:
			return addNext(lh,randkey,uri)
		randkey = ''.join([random.choice(rsrc) for x in xrange(2)]) + '+'
		lh = alreadyExists(randkey)
	return add(randkey,uri)


