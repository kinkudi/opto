from google.appengine.api import urlfetch
from Location import add
import webapp2
import urllib2
import re
import math

type_list = {
	'pdf':'pdf','ps':'ps','png':'image','jpeg':'image','jpg':'image','gif':'image','ico':'icon','dmg':'app',
	'exe':'app','crx':'app','deb':'app','app':'app','zip':'zip','gz':'zip','bzip':'zip','bz':'zip',
	'tar':'archive','cad':'cad','c':'c-code','py':'python-code',
'css':'stylesheet','js':'javascript','mpeg':'movie','mp3':'listen','wma':'listen','wmv':'watch','mov':'watch','cpp':'c++','cs':'c#','jar':'java-package','rb':'ruby','java':'java','pl':'perl','me':'readme','text':'text','txt':'text','rtf':'rtf','doc':'word-doc','odc':'openoffice-doc','ppt':'powerpoint','xz':'zip'}

status_humanizer = {
	'200':'success','405':'disallowed','404':'lost','301':'redirect','302':'redirect','304':'complete',
	'500':'serveroops','401':'urnotauthorized','402':'paymentrequired','403':'forbidden','2':'ok','3':'redirect',
	'4':'clienterror','5':'servererror','1':'infoonly'
}

language_humanizer = {
	'en':'english','zh':'chinese','hi':'hindi','ja':'japanese','pt':'portugese','es':'spanish',
	'vi':'vietnamese','ms':'malay','ko':'korean','he':'hebrew','iw':'hebrew','pa':'punjabi'
}

mime_humanizer = {
	'audio':'listen','image':'photo','message':'email','model':'3dmodel','multipart':'archival','video':'movie',
	'x-pk':'crypto','octet-stream':'binary','x-gz':'zip','x-ww':'form','x-dv':'dvi',
	'vnd.oasis.opendocument.text':'opendoc/text',
	'vnd.oasis.opendocument.spreadsheet':'opendoc/spreadsheet',
	'vnd.oasis.opendocument.presentation':'opendoc/presentation',
	'vnd.oasis.opendocument.graphics':'opendoc/drawing',
	'vnd.ms-excel':'excel',
	'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':'office/2007/file',
	'vnd.ms-powerpoint':'powerpoint',
	'application/vnd.openxmlformats-officedocument.presentationml.presentation':'powerpoint/2007',
	'application/msword':'word/doc',
	'application/vnd.openxmlformats-officedocument.wordprocessingml.document':'word/2007',
	'application/vnd.mozilla.xul+xml':'mozilla-xul',
	'application/vnd.google-earth.kml+xml':'googleearth-kml'
}

tlds = { 'www':1,'com':1,'edu':1,'net':1,'org':1,'gov':1 }


class Typify(webapp2.RequestHandler):
	def post(self):
		uri = self.request.POST['uri']
		domainpart,ftype = getURIInfo(uri)	
		result = None
		try:
			result = urlfetch.fetch(uri,method=urlfetch.HEAD,deadline=3,allow_truncated=True)
		except:
			pass
		if result is not None and result.status_code == 200:
			content_type = getType(result,uri)
			key = add(content_type,uri)
			self.response.out.write(key)
			return
		try:
			result = urlfetch.fetch(uri,deadline=3,allow_truncated=True)
		except:
			if ftype is None:		
				key = domainpart
			else:
				key = '/'.join([ftype,domainpart])
			key = add(key,uri)
			self.response.out.write(key)
			return
		if result is not None and result.status_code == 200:
			content_type = getType(result,uri)
			key = add(content_type,uri)
			self.response.out.write(key)
			return
		else:
			if ftype is None:		
				key = domainpart
			else:
				key = '/'.join([ftype,domainpart])
			key = add(key,uri)
			self.response.out.write(key)
			return

	def get(self):
		self.redirect('http://op.to/')

def humanize_type(content):
	if content is None:
		return
	info = content.split(';')
	mime = info[0].strip()
	if mime in mime_humanizer:
		return content
	bigpart,smallpart = mime.split('/')
	if bigpart in mime_humanizer:
		return mime_humanizer[bigpart]
	smallpre = smallpart[:4]
	if smallpre in mime_humanizer:
		return mime_humanizer[smallpre]
	if smallpart in mime_humanizer:
		return mime_humanizer[smallpart]
	if '-' in smallpart:
		smallparts = smallpart.split('-')
		if smallparts[0] == 'x':
			return '/'.join(smallparts[1:])
	return smallpart

def getURIInfo(uri):
	uriparts = uri.partition('//')
	urimain = uriparts[2].partition('/')	
	uridomain = urimain[0]
	ftype = urimain[2].rsplit('.',1)
	if len(ftype) > 1:
		ftype = ftype[-1]
		if ftype in type_list:
			ftype = type_list[ftype]
		elif len(ftype) > 5:
			ftype = None
	else:
		ftype = None
	domainpart = uridomain.split('.')
	domainpart.reverse()
	domainpart = '/'.join([d for d in domainpart[:3] if d not in tlds and len(d) > 2])
	return domainpart,ftype	

def getType(result,uri):
	domainpart,ftype = getURIInfo(uri)	
	status_code = str(result.status_code)
	status_code = status_humanizer.get(status_code)
	content_type = humanize_type(result.headers.get('Content-Type'))
	if content_type == None:
		if ftype == None:
			content_type = 'file/unknown'
	else:
		if ftype is not None:
			if ftype == content_type:
				ftype = None
	content_length = result.headers.get('Content-Length')
	if content_length is not None:
		content_length = int(content_length)
		if content_length > 1024: 	
			content_length = int(math.ceil(content_length/1024))
			if content_length > 1024:
				content_length = ''.join([`int(math.ceil(content_length/1024))`,'Mb'])
			else:
				content_length = ''.join([`content_length`,'Kb'])
		else:
			content_length = ''.join([`content_length`,'b'])	
	content_disp = result.headers.get('Content-Disposition')
	if content_disp is None:
		pass#content_disp = ''
	else:
		content_disp = 'download'
	content_modify = result.headers.get('Last-Modified')
	if content_modify is None:
		content_modify = 'ageless'
	else:	
		content_modify = ','.join(re.findall(r'[\w:]+',content_modify,re.UNICODE))
	content_language = result.headers.get('Content-Language')
	type_addr_list =[s for s in  [content_length,content_type,ftype,domainpart] if s is not None]
	return '/'.join(type_addr_list)

app = webapp2.WSGIApplication([
	(r'/t/.*',Typify)
	])

