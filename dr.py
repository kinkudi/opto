dic = {
# ISO 8859-1 Character Entities
'A' : "&Agrave;", 'A' : "&Aacute;", 'A' : "&Acirc;", 'A' : "&Atilde;", 'A' : "&Auml;", 'A' : "&Aring;",
'A' : "&AElig;", 'C' : "&Ccedil;",
'E' : "&Egrave;", 'E' : "&Eacute;", 'E' : "&Ecirc;", 'E' : "&Euml;",
'I' : "&Igrave;", 'I' : "&Iacute;", 'I' : "&Icirc;", 'I' : "&Iuml;",
'D' : "&ETH;", 'N' : "&Ntilde;",
'O' : "&Ograve;", 'O' : "&Oacute;", 'O' : "&Ocirc;", 'O' : "&Otilde;", 'O' : "&Ouml;", 'O' : "&Oslash;",
'U' : "&Ugrave;", 'U' : "&Uacute;", 'U' : "&Ucirc;", 'U' : "&Uuml;",
'Y' : "&Yacute;",
'T' : "&THORN;", 's' : "&szlig;",
'a' : "&agrave;", 'a' : "&aacute;", 'a' : "&acirc;", 'a' : "&atilde;", 'a' : "&auml;", 'a' : "&aring;",
'a' : "&aelig;", 'c' : "&ccedil;",
'e' : "&egrave;", 'e' : "&eacute;", 'e' : "&ecirc;", 'e' : "&euml;",
'i' : "&igrave;", 'i' : "&iacute;", 'i' : "&icirc;", 'i' : "&iuml;",
'e' : "&eth;", 'n' : "&ntilde;",
'o' : "&ograve;", 'o' : "&oacute;", 'o' : "&ocirc;", 'o' : "&otilde;", 'o' : "&ouml;", 'o' : "&oslash;",
'u' : "&ugrave;", 'u' : "&uacute;", 'u' : "&ucirc;", 'u' : "&uuml;",
'y' : "&yacute;", 't' : "&thorn;", 'y' : "&yuml;",
};
dr = {}
for j in dic.iteritems():
	dr[j[1]] = j[0]

import json
print json.dumps(dr)

